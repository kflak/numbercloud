#include "MiniBeeData.h"

MiniBeeData::MiniBeeData(){}

void MiniBeeData::setup(){
    port = 12345;
    numMBs = 8;
    for (int i = 0; i < numMBs; i++) {
        MiniBee mb;
        minibee.push_back(mb);
        prevValues.push_back(mb);
    };

    for (auto i : minibee) { i.setup(); };
    for (auto i : prevValues) { i.setup(); };

    idOffset = 9;
    receiver.setup(port);
}

void MiniBeeData::update(){
    while( receiver.hasWaitingMessages() ){
        ofxOscMessage m;
        receiver.getNextMessage(m);
        
        string address = ofToString("/minibee/data");
        if(m.getAddress() == address){
            int id = m.getArgAsInt(0);
            int idx = id - idOffset;
            float x = m.getArgAsFloat(1);
            float y = m.getArgAsFloat(2);
            float z = m.getArgAsFloat(3);

            minibee[idx].setX( x );
            minibee[idx].setY( y );
            minibee[idx].setZ( z );

            // copy over to vectors
            glm::vec3 current, prev;
            current.x = minibee[idx].getX();
            current.y = minibee[idx].getY();
            current.z = minibee[idx].getZ();
            prev.x = prevValues[idx].getX();
            prev.y = prevValues[idx].getY();
            prev.z = prevValues[idx].getZ();

            float prevDelta = prevValues[idx].getDelta();

            prevValues[idx].setX( x );
            prevValues[idx].setY( y );
            prevValues[idx].setZ( z );

            float delta = this->calcDelta(current, prev);
            minibee[idx].setDelta(delta);
            prevValues[idx].setDelta(delta);

            float alpha = 0.98;
            float deltaSmooth = this->calcDeltaSmooth(delta, prevDelta, alpha);
            minibee[idx].setDeltaSmooth(deltaSmooth);


        }
    };
}

MiniBee& MiniBeeData::getMiniBee(int id){
    return minibee[id - idOffset];
}

float MiniBeeData::calcDelta(glm::vec3 current, glm::vec3 previous){

    glm::vec3 deltas;
    float delta = 0;

    for (int i = 0; i < 3; i++){
        deltas[i] = abs( current[i] - previous[i] );
        delta += deltas[i];
    };

    delta = ofClamp(delta / 3, 0.0, 1.0);

    return delta;
}

float MiniBeeData::calcDeltaSmooth(float current, float previous, float alpha){

    return current + alpha * (previous - current);

}
