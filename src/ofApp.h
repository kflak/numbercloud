#pragma once

#include "ofMain.h"
#include "Particle.h"
#include "MiniBeeData.h"

class ofApp : public ofBaseApp{

public:
void setup();
void update();
void draw();
void resetParticles(); 

void keyPressed(int key);
void keyReleased(int key);
void mouseMoved(int x, int y );
void mouseDragged(int x, int y, int button);
void mousePressed(int x, int y, int button);
void mouseReleased(int x, int y, int button);
void mouseEntered(int x, int y);
void mouseExited(int x, int y);
void windowResized(int w, int h);
void dragEvent(ofDragInfo dragInfo);
void gotMessage(ofMessage msg);

string currentModeStr; 

ofVideoPlayer background;
// ofImage background;

vector <Particle> p;

float emissionRate;

MiniBeeData mbData;
};
