#pragma once
#include "ofMain.h"
#include "MiniBeeData.h"

class Particle{


public:
Particle();

void loadImage(string fileName );

void reset();
void update(float forceMul);
void draw();		

glm::vec3 pos;
glm::vec3 vel;
glm::vec3 frc;
glm::vec3 rotVel;

float degree;

float drag; 
float uniqueVal;

float scale;

ofImage image;
int alpha;

};
