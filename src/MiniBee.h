#pragma once

#include "ofMain.h"

class MiniBee {
public:
MiniBee();
float getX();
float getY();
float getZ();
float getDelta();
float getDeltaSmooth();
void setup();

void setX(float in);
void setY(float in);
void setZ(float in);
void setDelta(float in);
void setDeltaSmooth(float in);

private:
float x;
float y;
float z;
float delta;
float deltaSmooth;
};
