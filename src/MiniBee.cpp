#include "MiniBee.h"

MiniBee::MiniBee(){};

void MiniBee::setup(){
    x = 0.0;
    y = 0.0;
    z = 0.0;
    delta = 0.0;
    deltaSmooth = 0.0;
};

float MiniBee::getX(){ return x; };
float MiniBee::getY(){ return y; };
float MiniBee::getZ(){ return z; };
float MiniBee::getDelta(){ return delta; };
float MiniBee::getDeltaSmooth(){ return deltaSmooth; };

void MiniBee::setX(float in = 0.0){ 
    x = ofMap(in, 0.47020, 0.534, 0.0, 1.0, true); 
};
void MiniBee::setY(float in = 0.0){
    y = ofMap(in, 0.469, 0.5322, 0.0, 1.0, true); 
};
void MiniBee::setZ(float in = 0.0){
    z = ofMap(in, 0.469, 0.650, 0.0, 1.0, true); 
};
void MiniBee::setDelta(float in = 0.0){
    delta = in; 
};
void MiniBee::setDeltaSmooth(float in = 0.0){
    deltaSmooth = in; 
};
