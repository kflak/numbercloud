#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(true);

    mbData.setup();

    background.load("snowyBackground.mp4");
    background.play();

    string path = ofToString( "numbers" );
    ofDirectory dir(path); 
    string ext = ofToString( "png" );
    dir.allowExt(ext);
    dir.listDir();

    emissionRate = 5;
    int num = 100;

    p.assign(num, Particle());
    for (int i = 0; i < num; i++) {
        int index = int(ofRandom(dir.size()));
        p[i].loadImage(dir.getPath(index));
        p[i].scale = 40;
    };

    resetParticles();

}


//--------------------------------------------------------------
void ofApp::resetParticles(){

    for (unsigned int i = 0; i < p.size(); i++) {
        p[i].reset();
    }
}

//--------------------------------------------------------------
void ofApp::update(){

    background.update();
    mbData.update();

    float deltaSmooth = mbData.getMiniBee(9).getDeltaSmooth();
    deltaSmooth = glm::pow(deltaSmooth, 3) * 1000;
    for (unsigned int i = 0; i < p.size(); i++) {
        p[i].update(deltaSmooth);
    };

}

//--------------------------------------------------------------
void ofApp::draw(){

    background.draw(0, 0, ofGetWidth(), ofGetHeight());

    for (int i = 0; i < p.size(); i++) {
        p[i].draw();
    };
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if( key == 'q'){
        ofExit();
    }

    if( key == ' ' ){
        resetParticles();
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
