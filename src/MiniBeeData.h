#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "MiniBee.h"

class MiniBeeData {

public:
MiniBeeData();

void setup();
void update();

MiniBee& getMiniBee(int id);
float calcDelta(glm::vec3 current, glm::vec3 previous);
float calcDeltaSmooth(float current, float previous, float alpha);

int port;
vector <MiniBee> minibee;
vector <MiniBee> prevValues;
int idOffset;
int numMBs;

ofxOscReceiver receiver;
};
